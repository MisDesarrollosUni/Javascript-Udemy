function sumar(a, b) { // a y b son parámetros
    console.log(a + b);
}

sumar(5, 7); // 5 y 7 son argumentos

function saludar(nombre, apellido) {
    console.log(`Hola ${nombre} ${apellido}`);
}


saludar('Hector', 'Saldaña');
saludar('Hector');
saludar();