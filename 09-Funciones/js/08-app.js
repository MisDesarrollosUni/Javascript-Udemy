let total = 0;

function agregarCarrito(cantidad = 0) {
    return total += cantidad;
}

function calcularImpuesto(total) {
    return total * 1.16;
}


total = agregarCarrito(500);
total = agregarCarrito(100);
total = agregarCarrito(50);
total = agregarCarrito(50);


console.log(`Total: $${total}`);
console.log(`Total a pagar: $${calcularImpuesto(total)}`);