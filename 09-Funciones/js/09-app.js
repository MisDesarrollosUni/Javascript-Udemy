// Métodos de propiedad

const reproductor = {
    reproducir: function(cancion = 'desconocida') {
        console.log(`Reproduciendo canción: ${cancion}`);
    },
    pausar: function(id) {
        console.log(`Pausando...`);
    },
    crearPlaylist: (nombre = 'desconocido') => {
        console.log(`Creando playlist ${nombre}`);
    }
}

reproductor.reproducir('Who loves the sun');
reproductor.reproducir();

reproductor.pausar();

reproductor.crearPlaylist();
reproductor.crearPlaylist('Modo triste');