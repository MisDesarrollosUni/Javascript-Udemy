const aprender1 = function(tecnologia = 'algo') { // Expresión de función
    console.log(`Aprendiendo ${tecnologia}`);
}

/* Si tenemos solo una sola línea en el arrow, se pueden quitar las llaves y explicitamente 
se dice que esta retornando esa linea */

// si envias un parámetro a las arrow los parametros son opcionales
const aprender2 = tecnologia => `Aprendiendo ${tecnologia}`;


aprender1('JavaScript')
console.log(aprender2('páginas web'));