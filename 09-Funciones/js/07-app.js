// Cominicación en Funciones

iniciandoApp();

function iniciandoApp() {
    console.log('La aplicación se esta iniciando');
    segundaFuncion();
}


function segundaFuncion() {
    console.log('La aplicación ya se ha iniciado');
    autenticacion();
    autenticacion('Hector');
}


function autenticacion(nombre = 'Desconocido') {
    console.log(`Bienvenido ${nombre}`);
}