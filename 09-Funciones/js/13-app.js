// Arrow functions en métodos de propiedad con set y get

const reproductor = {
    cancion: '',
    reproducir: (cancion = 'desconocida') => console.log(`Reproduciendo canción: ${cancion}`),
    pausar: id => console.log(`Pausando...`),
    crearPlaylist: (nombre = 'desconocido') => console.log(`Creando playlist ${nombre}`),
    set nuevaCancion(cancion) {
        this.cancion = cancion
        console.log(`Canción ${cancion} añadida`);
    },
    get obtenerCancion() { console.log(`${this.cancion}`) }

}


reproductor.nuevaCancion = 'Mastter op puppets - Metallica'
reproductor.obtenerCancion
    /* 

    reproductor.reproducir('Who loves the sun');
    reproductor.reproducir();

    reproductor.pausar();

    reproductor.crearPlaylist();
    reproductor.crearPlaylist('Modo triste'); */