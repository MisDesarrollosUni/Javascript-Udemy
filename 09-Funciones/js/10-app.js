// Arrow functions

function aprender1() { // Declaración de función
    console.log(`Aprendiendo JavaScript`);
}

const aprender2 = function() { // Expresión de función
    console.log(`Aprendiendo JavaScript`);
}

/* Si tenemos solo una sola línea en el arrow, se pueden quitar las llaves y explicitamente 
se dice que esta retornando esa linea */
const aprender3 = () => {
    console.log(`Aprendiendo JavaScript`);
}

const aprender4 = () => `Aprendiendo JavaScript`;


aprender1()
aprender2()
aprender3();
console.log(aprender4());