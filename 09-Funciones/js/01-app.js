// Declaración de función ( Function Declaration )
function sumar1() {
    console.log(2 + 2);
}

sumar1();
sumar1();
sumar1();


// Expresión de función ( Function Expression )
const sumar2 = function() {
    console.log(3 + 3);
}

sumar2();
sumar2();
sumar2();