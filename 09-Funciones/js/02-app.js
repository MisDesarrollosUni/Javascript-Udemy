// Declaración de función ( Function Declaration )
sumar1();

function sumar1() {
    console.log(2 + 2);
}
/* Esta no dará error porque algo llamado hosting en JS, lee el documento y almacena las funciones, y la segunda leida
las ejecuta */


// Expresión de función ( Function Expression )
sumar2();

const sumar2 = function() {
        console.log(3 + 3);
    }
    /* En ese ejemplo, esta función es tomada como una variable entonces se toma en cuenta en la segunda leida, y no se puede 
       acceder porque fue llamada antes de su declaración */