// Parametros por default en funciones

// Solo se agregan en los paramatros valores que me gustrian colocar por defecto asignandosela
function saludar(nombre = 'Desconocido', apellido = '') {
    console.log(`Hola ${nombre} ${apellido}`);
}

saludar('Hector', 'Saldaña');
saludar('Hector');
saludar();