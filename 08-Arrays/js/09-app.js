const carrito = [
    { nombre: 'Pantalla', precio: 20000 },
    { nombre: 'Teclado', precio: 510 },
    { nombre: 'Bocina', precio: 800 },
    { nombre: 'Mouse', precio: 500 },
    { nombre: 'Celular', precio: 1000 },
    { nombre: 'Funda', precio: 200 },
    { nombre: 'Color', precio: 90 },
    { nombre: 'Lapiz', precio: 8 }
]

// Usando for para recorrer un arreglo
for (let i = 0; i < carrito.length; i++) {
    console.log(`${carrito[i].nombre} - Precio: ${carrito[i].precio}`);
}

console.log('------------------');

// Usando un metodo de arreglo llamado forEach, usando un metodo
carrito.forEach(function(producto) {
    console.log(`${producto.nombre} - Precio: ${producto.precio}`);
})

console.log('------------------');

// Usando un metodo de arreglo llamado forEach, usando una Arrow function
carrito.forEach((producto) => { console.log(`${producto.nombre} - Precio: ${producto.precio}`); })