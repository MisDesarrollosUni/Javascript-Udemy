const meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio']
    // A pesar que el arreglo es const, lo valores si se pueden modificar
meses[0] = 'Nuevo mes'

/* JS, ve que no tienes los valores anteriores entonces lo agrega 
en la poscición que le indicas */
meses[10] = 'Ultimo mes'

console.table(meses);