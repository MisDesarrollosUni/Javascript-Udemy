const meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio']

// Recorrer un arreglo
console.table(meses);
console.log(meses[0]);
console.log(meses[1]);


// Cuando mide el arreglo
console.log(meses.length);

for (let i = 0; i < meses.length; i++) {
    console.log(meses[i]);
}