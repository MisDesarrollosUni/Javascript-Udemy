// Objeto Object Literal
const producto = {
    nombre: "Monitor 30 pulgadas",
    precio: 300,
    disponible: true
}

//NUEVA FORMA DESTRUCTURING, CREA VARIABLE Y EXTRAE EL VALOR
let { disponible, precio, nombre } = producto


// Destructuring en arreglos
let numeros = [10, 20, 30, 40, 50];

const [primerElemento] = numeros;
console.log(primerElemento);

// Podemos extraer su valor dependiendo de la poscición
const [primero, segundo, tercero] = numeros;
console.log(primero);
console.log(segundo);
console.log(tercero);


/* Si queremos solo un valor en especifico, debemos de colocar comas, 
hasta llegar al valor que necesitemos */
const [, , , , quinto] = numeros;
console.log(quinto);


let numeros = [10, 20, 30, 40, 50];
/* Incluso podemos obtener uno, y todos los restantes tenerlos en otra variable spread */
const [primeroValor, ...ultimosValores] = numeros;
console.log(primeroValor); // 10
console.log(ultimosValores); // [20, 30, 40, 50]