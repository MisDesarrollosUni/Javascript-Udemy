const carrito = [];

const producto1 = { nombre: 'Pantalla', precio: 85 }

const producto2 = { nombre: 'Celular', precio: 90 }

const producto3 = { nombre: 'Teclado', precio: 80 }

/* En nuevas versiónes de JS, existen funciones que hacen lo mimos, se llaman
 - Declarativas  - NO modifica el objeto actual
 - Formas imperativas - Modifica el objeto actual*/


// FORMA DECLARATIVA (Spread Operator)

let resultado;

/* AGREGAR AL FINAL DEL ARREGLO
Hace una copia de carrito, toma el producto, y los almacena en resultado */
resultado = [...carrito, producto1]
    // Aqui se toma resultado porque tiene ya a producto 1
resultado = [...resultado, producto2]

/* AGREGAR AL INICIO DEL ARREGLO
Toma el producto, hace una copia de resultado con los objetos almacenados, y los une a la misma variable */

resultado = [producto3, ...resultado]

console.table(resultado);
console.table(carrito); // a carrito nunca fue agregado nada, si no que a resultado