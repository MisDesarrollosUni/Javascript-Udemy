const carrito = [
    { nombre: 'Pantalla', precio: 20000 },
    { nombre: 'Teclado', precio: 510 },
    { nombre: 'Bocina', precio: 800 },
    { nombre: 'Mouse', precio: 500 },
    { nombre: 'Celular', precio: 1000 },
    { nombre: 'Funda', precio: 200 },
    { nombre: 'Color', precio: 90 },
    { nombre: 'Lapiz', precio: 8 }
]


// Usando un metodo de arreglo llamado forEach, usando un metodo
const nuevoArrelo = carrito.forEach(function(producto) {
    return `${producto.nombre} - Precio: ${producto.precio}`;
})

console.log('------------------');


/* Lo que hace map, es que te puede retornar el mismo arreglo, obteniendolo en un nuevo arreglo
    ya sea para realizar filtros en arreglos y solo devolver aquellos que necesitas
 */
const nuevoArreglo2 = carrito.map(function(producto) {
    if (producto.precio >= 1000) return producto
        // return `${producto.nombre} - Precio: ${producto.precio}`;
})


console.log(nuevoArrelo);
console.log(nuevoArreglo2);