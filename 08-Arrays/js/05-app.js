const carrito = [];

const producto1 = {
    nombre: 'Pantalla',
    precio: 85
}

const producto2 = {
    nombre: 'Celular',
    precio: 90
}

const producto3 = {
    nombre: 'Teclado',
    precio: 80
}

// FORMA IMPERATIVA
// Agregar al final del arreglo

carrito.push(producto1)
carrito.push(producto2)

// FORMA IMPERATIVA
// Agregar al inicio del arreglo

carrito.unshift(producto3)

console.table(carrito);