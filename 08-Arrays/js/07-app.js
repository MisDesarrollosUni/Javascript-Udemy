const carrito = [];

const producto1 = {
    nombre: 'Pantalla',
    precio: 85
}

const producto2 = {
    nombre: 'Celular',
    precio: 90
}

const producto3 = {
    nombre: 'Teclado',
    precio: 80
}
const producto4 = {
    nombre: 'Mouse',
    precio: 80
}

carrito.push(producto1)
carrito.push(producto2)
carrito.push(producto4);
carrito.unshift(producto3)
console.table(carrito);

/* FORMA IMPERATIVA
Eliminar último elemento del arreglo */
carrito.pop();
console.table(carrito);

/* FORMA IMPERATIVA
Eliminar al inicio del arreglo */
carrito.shift();
console.table(carrito);


/* FORMA IMPERATIVA
Eliminar en medio del arreglo */
carrito.splice(1, 1); // requiere dos parametros, de donde a donde

console.table(carrito);