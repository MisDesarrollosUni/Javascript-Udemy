const numeros = [10, 20, 30, 40, 50, [1, 2, 3]]
console.table(numeros);

// Acceder al arreglo
// A uno en especial
console.log(numeros[4]);

// Al arreglo del arreglo
console.log(numeros[5][2]);