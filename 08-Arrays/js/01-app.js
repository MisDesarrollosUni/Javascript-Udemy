// Declarar arreglos
const numeros = [10, 20, 30];
// otra manera no tan utilizada
const meses = new Array('Enero', 'Febrero', 'Marzo');

console.log(numeros);
console.log(meses);


// Un arreglo con contiene datos de todo tipo
const deTodo = ["Hola", 14, false, null, 15.55, [1, 2, 3], { nombre: "Hector", edad: 19 }];
console.log(deTodo);