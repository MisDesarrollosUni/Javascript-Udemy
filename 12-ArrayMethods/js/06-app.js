const carrito = [
    { nombre: 'Monitor 27 Pulgadas', precio: 500 },
    { nombre: 'Televisión', precio: 100 },
    { nombre: 'Tablet', precio: 100 },
    { nombre: 'Audifonos', precio: 300 },
    { nombre: 'Teclado', precio: 400 },
    { nombre: 'Celular', precio: 700 },
]

/* every() todos los elementos deben cumplir la condicion para que nos devuelva un true
si algun elemento no la cumple retorna false */

// todos la cumplen TRUE
const resul = carrito.every(producto => producto.precio < 1000)
console.log(resul)

// solo algunos FALSE
const resul2 = carrito.every(producto => producto.precio < 500)
console.log(resul2)