const carrito = [
    { nombre: 'Monitor 27 Pulgadas', precio: 500 },
    { nombre: 'Televisión', precio: 100 },
    { nombre: 'Tablet', precio: 100 },
    { nombre: 'Audifonos', precio: 300 },
    { nombre: 'Teclado', precio: 400 },
    { nombre: 'Celular', precio: 700 },
]

/* Encontrar un elemento den el arreglo y me retorne ese objeto */

// con forEach()
let resultado = ''
carrito.forEach((product, indec) => {
    if (product.nombre === 'Tablet') {
        resultado = carrito[indec]
    }
})
console.log(resultado)

// con find() y solo trae el primer elemento que encuentre
let resultado2 = carrito.find(producto => producto.nombre === 100)
console.log(resultado2)