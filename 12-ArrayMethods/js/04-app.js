const carrito = [
    { nombre: 'Monitor 27 Pulgadas', precio: 500 },
    { nombre: 'Televisión', precio: 100 },
    { nombre: 'Tablet', precio: 100 },
    { nombre: 'Audifonos', precio: 300 },
    { nombre: 'Teclado', precio: 400 },
    { nombre: 'Celular', precio: 700 },
]

/* .filter() lo que hace, es retornarte un nuevo arreglo dada una coindicion */

let resultado;
resultado = carrito.filter(product => product.precio > 400) // retornar los productos mayores a 400 pesos
resultado = carrito.filter(product => product.precio < 600) // retornar los productos menores a 600 pesos
resultado = carrito.filter(product => product.nombre !== 'Audifonos') // retornar los productos que no son Audifonos
resultado = carrito.filter(product => product.nombre === 'Audifonos') // retornar los productos que son Audifonos

console.log(resultado);



