const meses = ['Enero', 'Febreo', 'Marzo', 'Abril']
const meses2 = ['Mayo', 'Junio', 'Julio', 'Agosto']
const meses3 = ['Septiembre', 'Octubre', 'Noviembre']

// concat(array1, array2, arrayn)

const resul = meses.concat(meses2, meses3, 'Diciembre')

console.log(resul);

// spread operator

const resul2 = [...meses, ...meses2, ...meses3, 'Diciembre'] // NO ...'Diciembre o asignara un nuevo elemento por letra
console.log(resul2)