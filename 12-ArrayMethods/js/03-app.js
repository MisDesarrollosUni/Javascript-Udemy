const carrito = [
    { nombre: 'Monitor 27 Pulgadas', precio: 500 },
    { nombre: 'Televisión', precio: 100 },
    { nombre: 'Tablet', precio: 100 },
    { nombre: 'Audifonos', precio: 300 },
    { nombre: 'Teclado', precio: 400 },
    { nombre: 'Celular', precio: 700 },
]

/* Sumar los precios */
/* let total = 0


carrito.forEach(producto => total += producto.precio)
console.log(total); */


/* Con reduce recibe dos argumentos, realiza la operacion y desde donde inicia*/
let resultado = carrito.reduce((acomulador, producto) => acomulador + producto.precio, 0)
console.log(resultado);

// 2100
// 2200


/*  Donde 
    acoumulador: es nuestra variable que almacena cada suma del objeto 
    producto: es cada objeto
    0 = es de donde empezará, se puede cambiar la inicialización
    */