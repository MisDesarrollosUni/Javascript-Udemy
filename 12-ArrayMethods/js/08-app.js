const meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio'];

const carrito = [
    { nombre: 'Monitor 27 Pulgadas', precio: 500 },
    { nombre: 'Televisión', precio: 100 },
    { nombre: 'Tablet', precio: 200 },
    { nombre: 'Audifonos', precio: 300 },
    { nombre: 'Teclado', precio: 400 },
    { nombre: 'Celular', precio: 700 },
]

// El spread operator lo que hace es que toma una copia del arreglo u objeto original sin modificarlo para nada
const meses2 = ['Agosto', ...meses]
console.log(meses2)


//  Se usan corchetes y ...
// y si importa el orden porque depende el como se iran agregando al arreglo

const nuevosProductos = [{
        nombre: 'Agregado 1',
        precio: 1500
    },
    {
        nombre: 'Agregado 2',
        precio: 1500
    }
]

const nuevoCarrito = [...nuevosProductos, ...carrito]
console.log(nuevoCarrito)