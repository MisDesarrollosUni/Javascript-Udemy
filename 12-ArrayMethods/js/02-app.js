const meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio'];

const carrito = [
    { nombre: 'Monitor 27 Pulgadas', precio: 500 },
    { nombre: 'Televisión', precio: 100 },
    { nombre: 'Tablet', precio: 100 },
    { nombre: 'Audifonos', precio: 300 },
    { nombre: 'Teclado', precio: 400 },
    { nombre: 'Celular', precio: 700 },
]


/* Encontrar el indice de un elemento con forEach */
meses.forEach((mes, indice) => {
    if (mes === 'Abril') {
        console.log(`Encontrado en el indice ${indice}`);
    }
})

/* findIndex() SOLO ENCUENTRA LA PRIMERA COINCIDENCIA Y RETORNA
NO BUSCA DEMÁS, SI NO EL PRIMERO QUE ENCUENTRA */


/* Encontrar el indice de un elemento don .findIndex() */
var indice = meses.findIndex(mes => mes === 'Abril')
console.log(indice);

/* Encontrar el indice de un arreglo de objetos don .findIndex() */
var indice2 = carrito.findIndex(producto => producto.precio === 100)
console.log(indice2);