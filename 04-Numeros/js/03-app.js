let resultado;

// PI
resultado = Math.PI;

// Redondear <=4 Abajao | >= 5 Arriba
resultado = Math.round(2.5)

// Reondear arriba
resultado = Math.ceil(2.1)

// Redondear abajo
resultado = Math.floor(2.9)

// Raíz cuadrada
resultado = Math.sqrt(64)

// Potencia, número , a que potencia
resultado = Math.pow(3,3)

// Valor absoluto
resultado = Math.abs(-3423)

// Valor minimo de un rango
resultado = Math.min(3,345,6,6,4,76,234,356345,34554,)

// Valor maximo de un rango
resultado = Math.max(34,343,52,344534,5345345623,2323,2)

// Valor aleatorio
resultado = Math.random()*20;

// Valo aleatorio recomendado, toma valores del 0 al 5
resultado = Math.floor(Math.random()*6)
console.log(resultado);