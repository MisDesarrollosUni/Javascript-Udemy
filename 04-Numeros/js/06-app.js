const num1 = 20
const num2 = '29'
const num3 = 'Diez'
const num4 = '23.2223'

// Convertir String a números
console.log(Number.parseInt(num2));
console.log(Number.parseFloat(num4));

// Revisar si un número es entero o no
console.log(Number.isInteger(num3));
console.log(Number.isInteger(num1));