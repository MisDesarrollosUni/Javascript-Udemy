// OBJETOS ANIDADOS
const producto = {
    //PROPIEDAD O LLAVE DEL OBJETO
    nombre: "Monitor 30 pulgadas",
    precio: 300,
    disponible: true,
    informacion: {
        medidas: {
            peso: "1 kg",
            medida: "1 m"
        },
        fabricacion: {
            pais: "China"
        }
    }
}

//DESTRUCTURING EN OBJETOS ANIDADOS

let { nombre, informacion: { medidas: { peso, medida } } } = producto
//                             |          |      |
console.log(nombre);
console.log(peso);
console.log(medida);
//medida no existe porque le decimos que tiene mas prioridad lo que tiene dentro de ella misma
console.log(medidas);