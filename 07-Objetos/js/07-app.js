
const producto = {
    nombre: "Monitor 30 pulgadas",
    precio: 300,
    disponible: true,
}

//UN OBJETO DECLARADO AUN CON CONST, SI SE PUEDE CAMBIAR SU VALOR

producto.nombre = "OTRO MONITOR"

console.log(producto);