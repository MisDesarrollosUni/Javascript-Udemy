
//UNIR DOS OBJETOS EN UNO SOLO
//UNIR DOS OBJETOS EN UNO SOLO
//UNIR DOS OBJETOS EN UNO SOLO

const producto = {
    nombre: "Monitor 30 pulgadas",
    precio: 300,
    disponible: true,
}

const medidas = {
    peso: "1 kg",
    medida: "1 m";
}

const resultado = Object.assign(producto, medidas);
const resultado2 = { ...producto, ...medidas}; //ESTAS ES MAS USADA

console.log(resultado);
console.log(resultado2);