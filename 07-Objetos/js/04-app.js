// OBJECT LITERAL
const producto ={
    //PROPIEDAD O LLAVE DEL OBJETO
    nombre: "Monitor 30 pulgadas",
    precio: 300,
    disponible: true
}


// EXTRAER VARIABLES DEL OBJETO

//VIEJA FORMA
var nombre = producto.nombre;
var precio = producto.precio;
var disponible = producto.disponible
console.log(nombre);
console.log(precio);
console.log(disponible);



//NUEVA FORMA DESTRUCTURING, CREA VARIABLE Y EXTRAE EL VALOR
// las variables que estan dentro de las llaves, deben ser las mismas del objeto
let { nombre, precio, disponible, noExiste } = producto
console.log(nombre);
console.log(precio);
console.log(disponible);
console.log(noExiste);