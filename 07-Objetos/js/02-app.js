// OBJECT LITERAL
const producto ={
    //PROPIEDAD O LLAVE DEL OBJETO
    nombre: "Monitor 30 pulgadas",
    precio: 300,
    disponible: true
}

console.log(producto);
console.log(producto.nombre);
console.log(producto.precio);
console.log(producto.disponible);

console.log(producto['nombre']);
console.log(producto['precio']);
console.log(producto['disponible']);

