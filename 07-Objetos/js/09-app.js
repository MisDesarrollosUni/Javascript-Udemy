

//CON ESTO SE DICE QUE ESTE DOCUMENTO JS, SE DEBE USAR LA SINTAXIS ESTRICTA COMO UN LENGUAJE COMO TAL
"use strict";
const producto = {
    nombre: "Monitor 30 pulgadas",
    precio: 300,
    disponible: true,
}


// CON SEAL NO SE PUEDEN AGREGAR MAS ELEMENTOS NI BORRAR
// PERO SI SE PUDEN CAMBIAR LOS VALORES DE LAS VARIABLES
Object.seal(producto);

 producto.nombre = "otro nombre";
 producto.precio = 343434
 //producto.nuevoAtributo = 32432;
// delete producto.disponible;
// LO COMENTADO DARA ERROR


// SABER SI UN OBJETO ESTA CONGELADO

if(Object.isSealed(producto)){
    console.log("SI ESTA SELLADO");
}else{
    console.log("NO ESTA SELLADO");

}