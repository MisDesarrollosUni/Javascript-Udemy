

//CON ESTO SE DICE QUE ESTE DOCUMENTO JS, SE DEBE USAR LA SINTAXIS ESTRICTA COMO UN LENGUAJE COMO TAL
"use strict";

const producto = {
    nombre: "Monitor 30 pulgadas",
    precio: 300,
    disponible: true,
}


// CONGELAR UN OBJETO PARA QUE NO PUEDA CAMBIAR SU VALOR
Object.freeze(producto);

// producto.nombre = "otro nombre";
// producto.precio = 343434
// producto.nuevoAtributo = 32432;
// delete producto.disponible;
//SIEMPRE SERA ERROR


// SABER SI UN OBJETO ESTA CONGELADO

if(Object.isFrozen(producto)){
    console.log("SI ESTA CONGELADO");
}else{
    console.log("NO ESTA CONGELADO");

}