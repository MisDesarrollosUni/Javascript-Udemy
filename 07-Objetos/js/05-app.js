// OBJETOS ANIDADOS
const producto = {
    //PROPIEDAD O LLAVE DEL OBJETO
    nombre: "Monitor 30 pulgadas",
    precio: 300,
    disponible: true,
    informacion: {
        medidas: {
            peso: "1 kg",
            medida: "1 m"
        },
        fabricacion: {
            pais: "China"
        }
    }
}

console.log(producto.informacion.fabricacion.pais);

