
// OBJECT LITERAL 
const producto = {
    nombre: "Monitor 30 pulgadas",
    precio: 300,
    disponible: true,
}

// OBJECT CONSTRUCTOR 

function Producto(nombre, precio){
    this.nombre = nombre;
    this.precio = precio;
    this.disponible = true;
}

const prod1 = new Producto("Moto G6 plus", 6000);
const prod2 = new Producto("iPhone 12", 24000);

console.log(prod1);
console.log(prod2);