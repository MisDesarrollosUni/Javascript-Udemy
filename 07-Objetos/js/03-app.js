// OBJECT LITERAL
const producto ={
    //PROPIEDAD O LLAVE DEL OBJETO
    nombre: "Monitor 30 pulgadas",
    precio: 300,
    disponible: true
}

//AGREGAR UNA NUEVA PROPIEDAD O VARIABLE AL OBJETO
producto.imagen = "imagen.jpg"

//ELIMINAR UNA PROPIEDAD O VARIABLE DEL OBJETO

delete producto.disponible;

console.log(producto);