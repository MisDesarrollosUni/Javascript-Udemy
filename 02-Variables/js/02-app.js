// ASGINAR VARIABLES CON LET
// Utilizar mejor let y const ahora con las as usadas, var es la forma más vieja

let producto = 'Papas';

// Reasignar un valor a un producto
producto = 'Coca';
producto = null;
producto = 300;

console.log(producto);
