// ASIGNAR VARIBALES CON VAR

// CamelCase | La mas utilizada
var holaMundo = "Hola Mundo";

//UnderScore o Snake
var hola_mundo = "Hola Mundo";

//Pascal Case | Utilizada para clases
var Hola, 
    Mundo;

// Variables que contengan cadera de String son con comillas simple '' 