function Cliente(nombre, saldo) {
    this.nombre = nombre;
    this.saldo = saldo;
}

//* Prototype es darle metodos a las clases
//! No usar arrow function, se usa function, para hacer referencia al mismo objeto
Cliente.prototype.tipoCliente = function () {
    let tipo;
    if (this.saldo > 10000) {
        tipo = "Gold";
    } else if (this.saldo > 5000) {
        tipo = "Platinium";
    } else {
        tipo = "Normal";
    }
    return tipo;
};

Cliente.prototype.nombreClienteSaldo = function () {
    return `Nombre: ${this.nombre} Saldo: ${
        this.saldo
    } Tipo cliente: ${this.tipoCliente()}`;
};

Cliente.prototype.retirarSaldo = function (retirar) {
    this.saldo -= retirar;
};

const pedro = new Cliente("Pedro", 6000);
console.log(pedro.tipoCliente());
console.log(pedro.nombreClienteSaldo());
pedro.retirarSaldo(1000)
console.log(pedro.nombreClienteSaldo());
console.log(pedro);
