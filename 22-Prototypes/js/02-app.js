/*
 * Prototypes
 * Se pueden instanciar y usar muchas veces
 */
function Cliente(nombre, saldo) {
    this.nombre = nombre;
    this.saldo = saldo;
}

const hector = new Cliente("Hector Saldaña", 8000);
console.log(hector);

function formatearClient(cliente) {
    const { nombre, saldo } = cliente;
    return `El cliente ${nombre} tiene un saldo de $ ${saldo}`;
}

console.log(formatearClient(hector));





function Empresa(nombre, saldo, categoria) {
     this.nombre = nombre;
     this.saldo = saldo;
     this.categoria = categoria;
 }

 function formatearEmpresa(empresa) {
     const { nombre, saldo,categoria } = empresa;
     return `El cliente ${nombre} tiene un saldo de $ ${saldo} y la categoría ${categoria}`;
 }

 const EH = new Empresa('SVAM', 8000, 'Tecnología')
 console.log(formatearEmpresa(EH))