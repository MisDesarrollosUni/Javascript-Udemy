/* 
     * Un object literal
     * No se puede instanciar
*/
const cliente = {
     nombre: 'Hector',
     saldo: 500
}

console.log(cliente);




/*
     * Prototypes
     * Se pueden instanciar y usar muchas veces
*/


function Cliente(nombre, saldo) {
     this.nombre = nombre
     this.saldo = saldo
}

const hector = new Cliente('Hector Saldaña', 8000)
console.log(hector)
