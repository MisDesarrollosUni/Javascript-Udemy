function Cliente(nombre, saldo) {
     this.nombre = nombre;
     this.saldo = saldo;
 }
 
 //* Prototype es darle metodos a las clases
 //! No usar arrow function, se usa function, para hacer referencia al mismo objeto
 Cliente.prototype.tipoCliente = function () {
     let tipo;
     if (this.saldo > 10000) {
         tipo = "Gold";
     } else if (this.saldo > 5000) {
         tipo = "Platinium";
     } else {
         tipo = "Normal";
     }
     return tipo;
 };
 
 Cliente.prototype.nombreClienteSaldo = function () {
     return `Nombre: ${this.nombre} Saldo: ${
         this.saldo
     } Tipo cliente: ${this.tipoCliente()}`;
 };
 
 Cliente.prototype.retirarSaldo = function (retirar) {
     this.saldo -= retirar;
 };

 // ?----------------------------------------------------------------

 function Persona(nombre, saldo, telefono) {
      Cliente.call(this, nombre, saldo) //* Heredar los atributos
    /*  this.nombre = nombre;
     this.saldo = saldo; */
     this.telefono = telefono; // * Y agregamos uno mas
 }

 //* Heredar tambien los metodos
 Persona.prototype = Object.create(Cliente.prototype)
 Persona.prototype.constructor = Cliente;

 Persona.prototype.mostrarTelefono = function(){
      return `El telefono es ${this.telefono}`
 }

 const hector = new Persona('Hector Saldaña', 5000, 7772003900)
 console.log(hector)
 console.log(hector.nombreClienteSaldo())
 console.log(hector.mostrarTelefono())

 