let num1 = 20
let num2 = '40'
let num3 = 12
let num4 = '20'

// Compara el valor
console.log(num1 == num4);

// Este operador es mas estricto compara valor y el tipo de dato que son

console.log(typeof num1);

console.log(typeof num4);

console.log(num1 === parseInt(num4));

console.log(num1 != num4);
// Lo mismo pasa
console.log(num1 !== num4);