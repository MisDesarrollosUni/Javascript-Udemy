const pendientes = ['Tarea', 'Computadora', 'Actividad', 'JavaScript', 'Script', 'React']

/* Itera sobre arreglos */
for (let pendient of pendientes) {
    console.log(pendient);
}


/*  Itera sobre objetos  */

const auto = {
    modelo: 'Camaro',
    year: 2021,
    motor: '4.0'
}


for (let partes in auto) {
    console.log(`${partes}`);
    console.log(`${auto[partes]}`);
}

console.log('______________________________');

/* Lo mismo de iterar sobre objetos pero con nueva implementación de ECMAScript7 */
for (let [llave, valor] of Object.entries(auto)) {

    console.log(llave);
    console.log(valor);
}

