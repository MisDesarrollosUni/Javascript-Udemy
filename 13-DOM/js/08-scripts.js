// * TRAVERSING THE DOM Navegar entre tus elementos
const navegacion = document.querySelector('.navegacion');
console.log(navegacion.childNodes); // Los espacios en blanco son considerados elementos 
console.log(navegacion.children) // Muestra los hijos de ese elemento

console.log(navegacion.firstElementChild);
console.log(navegacion.lastElementChild);