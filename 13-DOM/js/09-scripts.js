// * Eliminar elementos del DOM

// ? Eliminar a elemento por si mismo
const primerEnlace = document.querySelector('a');
primerEnlace.remove();
console.log(primerEnlace);

//? El padre elimina el hijo
const navegacion = document.querySelector('.navegacion');
console.log(navegacion.children); // Nos muestra los nodos del padre
navegacion.removeChild(navegacion.children[1]); // Eliminara el hijo de su indice
console.log(navegacion.children);