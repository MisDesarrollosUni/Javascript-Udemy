// ? querySelector solamente trae el primero que encuentre
// ? (.) para clases y (#) para id's

const card = document.querySelector('.card');
console.log(card);

// ? Podemos tener selectores como en CSS
const info = document.querySelector('.premium .info');

// ? Seleccionar el 2do card de hospedaje
const segundoCard = document.querySelector('section.hospedaje .card:nth-child(2)')
console.log(segundoCard);

// ? Seleccionar el formulario
const formulario = document.querySelector('.contenido-hero #formulario')
console.log(formulario);

// ? Seleccionar etiqueas HTML
const navegacion = document.querySelector('nav')
console.log(navegacion);