 const encabezado = document.querySelector('h1');

 // ? AGREGAR ESTILOS DESDE JS 
 encabezado.style.backgroundColor = 'red';
 encabezado.style.textTransform = 'uppercase';
 console.log(encabezado);

 // * AGREGAR CLASES DESDE JS Y REMOVER CLASES*
 // ? classList contiene un arreglo de todas las clases que utiliza css
 const card = document.querySelector('.card');
 card.classList.add('MiClase', 'MiOtraClase');
 card.classList.remove('card')
 console.log(card.classList);