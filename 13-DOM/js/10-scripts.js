// * Generar HTML en JS
const enlace = document.createElement('A'); // Se le pasa la etiqueta en mayus o minus
enlace.textContent = 'Nuevo enlace' // Agregdar texto
enlace.href = '/nuevo-enlace' // Agregar enlace

console.log(enlace);

// Seleccionar en donde lo vamos a insertar en
const navegacion = document.querySelector('.navegacion');
// ? Agregar un hijo nuevo a la navegacion, pero lo agrega al final de los hijos
navegacion.appendChild(enlace)

// ? Insertarlo en donde tu quieras
navegacion.insertBefore(enlace, navegacion.children[1]);
// Recibe dos argumentos, en elemento y que posicion del elemento estará




// * Crear un card de forma dinamica
const parrafo1 = document.createElement('p');
parrafo1.textContent = 'Concierto';
parrafo1.classList.add('categoria', 'concierto')

const parrafo2 = document.createElement('p');
parrafo2.textContent = 'Concierto de Rock'
parrafo2.classList.add('titulo')

const parrafo3 = document.createElement('p');
parrafo3.textContent = '$800 po persona'
parrafo3.classList.add('precio')

// Div con la clase de info
const info = document.createElement('div')
info.classList.add('info');

info.appendChild(parrafo1);
info.appendChild(parrafo2);
info.appendChild(parrafo3);

// Crear imagen
const imagen = document.createElement('img')
imagen.src = 'img/hacer2.jpg'
imagen.alt = 'Texto alternativo'

// Crear card
const card = document.createElement('div')
card.classList.add('card')

// Asignar informacion
card.appendChild(imagen);
card.appendChild(info);


// insertar HTML
const contenedor = document.querySelector('.hacer .contenedor-cards')
contenedor.appendChild(card);
contenedor.insertBefore(card, contenedor.children[0]); // Los dos agregan al inicio
contenedor.insertBefore(card, contenedor.firstChild);

console.log(card);