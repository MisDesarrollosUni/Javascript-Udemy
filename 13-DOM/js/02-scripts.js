// ! SELECCIONAR ELEMENTOS POR SU CLASE
// ? SELECCIONAR ELEMENTOS POR SU CLASE
// // SELECCIONAR ELEMENTOS POR SU CLASE
// TODO SELECCIONAR ELEMENTOS POR SU CLASE
// * SELECCIONAR ELEMENTOS POR SU CLASE
const header = document.getElementsByClassName('header');
console.log(header);

const hero = document.getElementsByClassName('hero')
console.log(hero)


// SI LAS CLASES TIENEN MÁS DE UN ELEMENTO
const contenedores = document.getElementsByClassName('contenedor');
console.log(contenedores)

// SI UNA CLASE NO EXISTE
const noExiste = document.getElementsByClassName('no-existe')
console.log(noExiste);