// ! Text
// ? Text
// // Text
// TODO Text
// * Text

// TODO Eventos en el formulario de

// * Seleccionar formulario de dos maneras
// * 1ra, arroy function
const formulario = document.querySelector('#formulario');
 formulario.addEventListener('submit',(e) => {
     e.preventDefault(); // ! Es hacer que el boton submit no lo envie por default, si no que lo colocamos para hacer las validaciones que neceitemos, antes de enviarlo
     console.log(e);
})  

// * 2da, declaration function
formulario.addEventListener('submit',validarFormulario) // ? Ya sabe que le tiene que enviar el evento
function validarFormulario(e){ // ? y por eso lo recibimos
     e.preventDefault(); 
     console.log(e);
}