const nav =document.querySelector('.navegacion')

// TODO Eventos con el MOUSE

// ? Cuando entra al elemento
nav.addEventListener('mouseenter', ()=>{
     console.log('Entrando en la navegacion')
})

// ? Cuando sale del elemento
nav.addEventListener('mouseout', ()=>{
     console.log('Saliendo en la navegacion')
})


// ? mousedown es similar a un click
nav.addEventListener('mousedown', ()=>{
     console.log('Click en la navegacion')
})

// ? Dar doble click
nav.addEventListener('dblclick', ()=>{
     console.log('Doble click en la navegacion')
})