const busqueda = document.querySelector('.busqueda')
// TODO Eventos con el TECLADO
/*
// ? Se activa al presionar una tecla
busqueda.addEventListener('keydown',() =>{
     console.log('escribiendo')
})

// ? Se activa al dejar de presionar una tecla
busqueda.addEventListener('keyup',() =>{
     console.log('Deje de presionar')
})

// ? Se activa cuando estas en el input y sales de el
busqueda.addEventListener('blur',() =>{
     console.log('Sali del input')
})

// ? Se activa cuando copias texto del input
busqueda.addEventListener('copy',() =>{
     console.log('Copie el texto')
})

// ? Se activa cuando pegas texto del input
busqueda.addEventListener('paste',() =>{
     console.log('Pegue texto')
})

// ? Se activa cuando cortas texto del input
busqueda.addEventListener('cut',() =>{
     console.log('Pegue texto')
})
*/
/**
 * // Hace todos los eventos anteriores salvo el blur
 */
busqueda.addEventListener('input',(event) =>{
     console.log(event.target.value) // * tenemos el evento y podemos acceder a sus propiedades
})