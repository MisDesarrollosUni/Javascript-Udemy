// TODO Event Bubbling
// ? Evita la propagación de eventos cuando esta uno dentro de otro elemento, y solo quieres usar un evento
const cardDiv = document.querySelector(".card");
const infoDiv = document.querySelector(".info");
const titulo = document.querySelector(".titulo");

cardDiv.addEventListener("click", (e) => {
  e.preventDefault();
  console.log("Card");
});

infoDiv.addEventListener("click", (e) => {
  e.preventDefault();
  console.log("info");
});

titulo.addEventListener("click", (e) => {
  e.preventDefault();
  console.log("titulo");
});
