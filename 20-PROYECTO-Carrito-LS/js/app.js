// * Variables
const carrito = document.querySelector("#carrito");
const contenedorCarrito = document.querySelector("#lista-carrito tbody");
const vaciarCarritoBtn = document.querySelector("#vaciar-carrito");
const listaCursos = document.querySelector("#lista-cursos");
let articulosCarrito = [];

agregarEventsListeners();
function agregarEventsListeners() {
    // ? Cuando agregar un curso presionando "Agregar al carrito"
    listaCursos.addEventListener("click", agregarCurso);

    // ? Elimina cursos del carrito
    carrito.addEventListener("click", eliminarCurso);

    // ? Vaciar carrito
    vaciarCarritoBtn.addEventListener("click", () => {
        articulosCarrito = [];
        limpiarHTML();
    });

    // ? Al cargar pagina
    document.addEventListener("DOMContentLoaded", () => {
        articulosCarrito = JSON.parse(localStorage.getItem("carrito")) || [];
        carrtioHTML();
    });
}

// * Funciones

function agregarCurso(e) {
    e.preventDefault(); // TODO Evitamos que vaya hacia el elementos seleccionado
    if (e.target.classList.contains("agregar-carrito")) {
        // TODO Verificamos si fue al boton, al que dio click
        const cursoSeleccionado = e.target.parentElement.parentElement;
        leerDatosCurso(cursoSeleccionado);
    }
}

function eliminarCurso(e) {
    e.preventDefault();
    if (e.target.classList.contains("borrar-curso")) {
        const cursoid = e.target.getAttribute("data-id");
        articulosCarrito = articulosCarrito.filter(
            (curso) => curso.id !== cursoid
        );
        carrtioHTML();
    }
}

// * Lee la información del HTML al que le dimos click y extrae la información del curso
function leerDatosCurso(curso) {
    const infoCurso = {
        imagen: curso.querySelector("img").src,
        titulo: curso.querySelector("h4").textContent,
        precio: curso.querySelector(".precio span").textContent,
        id: curso.querySelector("a").getAttribute("data-id"),
        cantidad: 1,
    };

    // ? Revisa si un elemento ya esta en el carrito
    const existe = articulosCarrito.some((curso) => curso.id === infoCurso.id);
    if (existe) {
        const cursos = articulosCarrito.map((curso) => {
            if (curso.id === infoCurso.id) {
                curso.cantidad++; // TODO retorna el objeto actualizado
                return curso;
            } else {
                return curso; // TODO retorna objetos que no son actualizados
            }
        });
        articulosCarrito = [...cursos];
    } else {
        // TODO agregar elementos al arreglo del carrito
        articulosCarrito = [...articulosCarrito, infoCurso];
    }
    carrtioHTML();
}

// * Muestra el carrito de compras en el HTML
function carrtioHTML() {
    // ? Limpiar el HTML
    limpiarHTML();

    // ? Reccore carrito y genera el HTML
    articulosCarrito.forEach((curso) => {
        const { imagen, titulo, precio, cantidad, id } = curso;
        const row = document.createElement("tr");
        row.innerHTML = `
          <td><img src="${imagen}" width="100"/></td>
          <td>${titulo}</td>
          <td>${precio}</td>
          <td>${cantidad}</td>
          <td>
               <a href="" class="borrar-curso" data-id="${id}"> X </a>
          <td/>
         `;
        // TODO agrega el HTML del carrito en nuestro tbody
        contenedorCarrito.appendChild(row);
    });

    sincronizarStorage();
}

function sincronizarStorage() {
    localStorage.setItem("carrito", JSON.stringify(articulosCarrito));
}

function limpiarHTML() {
    // ? Forma lenta
    //     contenedorCarrito.innerHTML = "";

    // ? Mejor forma
    while (contenedorCarrito.firstChild) {
        contenedorCarrito.removeChild(contenedorCarrito.firstChild);
    }
}
