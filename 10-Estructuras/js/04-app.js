// OBJECT LITERAL
const producto = {
        nombre: 'Pantalla Smart TV',
        marca: 'Samsung',
        precio: 6999.0,
        stock: 40,
        color: 'Negro',
        pulgadas: '40'
    }
    // DESTRUCTURING
let { nombre, precio, stock, pulgadas } = producto;

// TEMPLATE LITERALS
console.log(`${nombre} cuesta $${precio} MXN`);
// Consola: Pantalla Smart TV cuesta $6999 MXN 

// OPERADORES COMPARATIVOS ESTRICTOS
if (stock == pulgadas) {
    console.log('\nCon == son iguales porque compara contenido');
    if (stock === pulgadas) {

    } else {
        console.log('Con === no son iguales porque ademas de comparar contenido, compara tipo de dato');
    }
}
console.log(`\nLa variable pulgadas es ${typeof pulgadas}`); // La variable pulgadas es string
console.log(`La variable stock es ${typeof stock}`); // La variable stock es number