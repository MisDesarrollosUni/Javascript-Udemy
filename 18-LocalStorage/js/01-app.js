// * Guardar un dato, en localStorage se basa en clave-valor y solo guarda string
localStorage.setItem('nombre', 'Hector Saldaña Espinoza')

const producto = {
     nombre: 'Pantalla Smart',
     precio: 3000
}

// ? Pasar un objeto a string para guardarlo
const productoString = JSON.stringify(producto); 
localStorage.setItem('producto', productoString);

const meses = ['Enero', 'Febrero', 'Marzo']
localStorage.setItem('meses', JSON.stringify(meses));
