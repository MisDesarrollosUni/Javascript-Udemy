// Operador termario
const bool = false;

// Condicion ? Verdadero : Falso
console.log((bool) ? "Es verdadero" : "Es falso");

// Esto cofunciona como un if, pero en menor escala

if (bool) {
    console.log("Es verdadero");
} else {
    console.log("Es falso");
}