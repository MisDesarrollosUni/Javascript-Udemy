const bool1 = true;
const bool2 = false;
const bool3 = "true";
const bool4 = new Boolean(false);

console.log(bool1);
console.log(bool2);
console.log(bool3);
console.log(bool4);


// En booleanos no compara contenido si no, si los compara correctamente con
//  dos igual == 
console.log(bool1 == bool3);