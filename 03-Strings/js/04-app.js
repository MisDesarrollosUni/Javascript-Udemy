// ELIMINAR ESPACIONS EN BLANCO

const texto = '                hola                 ';

// nueva forma
console.log(texto);

// al inicio
console.log(texto.trimStart());
// al final
console.log(texto.trimEnd());
// en ambas
console.log(texto.trimStart().trimEnd());


// otra forma en ambas (es más vieja)
console.log(texto.trim());
