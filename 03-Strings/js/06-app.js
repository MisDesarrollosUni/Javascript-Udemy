
// .repeat repetir una cadena de texto
console.log('Repetir');
const texto = 'hola como estas, '.repeat(3);
// si el argumento es decimal este lo redondea para dejarlo de forma entera
const texto2 = 'hola como estas, '.repeat(4.5);
console.log(texto);
console.log(texto2);
console.log('');

// .split para dividir un texto, dado un argumento
console.log('Dividir');
const nombre = 'H e c t o r';
console.log(nombre.split(' '));
const nombre1 = 'H%e%c%t%o%r';
console.log(nombre.split('%'));