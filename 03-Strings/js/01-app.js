const producto1 = 'Hector Saldaña Espinoza';
const producto2 = "Hector Saldaña Espinoza";
const producto3 = String('Hector Saldaña Espinoza');

// Te dice las posciciones de cada caracteres en el String
const producto4 = new String('Hector Saldaña Espinoza');  

// Escapar las comillas \"" para mostrar en pantalla, los caracteres de comillas u otros
const producto5 = "Pantalla de 20\"";
const producto5 = "Pantalla de 20""; 
// Error arriba

console.log(producto1);
console.log(producto2);
console.log(producto3);
console.log(producto4);
console.log(producto5);