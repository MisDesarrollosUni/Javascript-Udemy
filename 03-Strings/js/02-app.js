// MÉTODOS PARA LOS STRING O CADENAS DE TEXTO

const nombre = 'Hector Saldaña Espinoza'
console.log(nombre);

// Conocer el número de letras en la cadena
console.log(nombre.length);

// Conocer si el String contiene la letra o palbara, te puede retornar 
// la poscicion o -1 si no existe
console.log('indexOF');
console.log(nombre.indexOf('Saldaña'));
console.log(nombre.indexOf('Perez'));

// Hace lo mismo pero regresa false o true y no distingue entre caracter(es) MAYÚS o minús
console.log('includes');
console.log(nombre.includes('Saldaña'));
console.log(nombre.includes('Perez'));