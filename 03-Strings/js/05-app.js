const texto = 'hola como estas';

// .replace para remplazar
console.log('Remplazar');
console.log(texto.replace('estas','te encuentras'));
console.log('');

// .slice para cortar
console.log('Slice');
console.log(texto.slice(0,5));
// se confunde porque estas yendo hacia atrás y eso no se puede con slice
console.log(texto.slice(5,0));
console.log('');

// alternativa .substring a .slice
console.log('Substring');
console.log(texto.substring(0,5));
// es mas listo e invirtie los valores si el primer argumento es mayor al segundo
console.log(texto.substring(5,0));
console.log('');

// .charAt para extraer una letra
console.log('Extraer una letra');
console.log(texto.substring(0,1));
console.log(texto.charAt(0));