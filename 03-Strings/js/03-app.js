
// CONCATENAR STRINGS
const nombre = 'Hector'
const primerApe = ' Saldaña'
const segundoApe = ' Espinoza'
console.log('Formas de concatenación');
console.log(nombre.concat(primerApe, segundoApe));
console.log(nombre + primerApe + segundoApe);
console.log(nombre + ' Saldaña' + segundoApe);
console.log(nombre , ' Saldaña' , segundoApe);

// Concatenar con templare literals usando la comilla invertida (BACKTICK) alt + 96 ``
console.log('');
console.log('Template Literals');
console.log(`${nombre} ${primerApe} ${segundoApe}`);