//* Variables
const formulario = document.querySelector("#formulario");
const listaTweets = document.querySelector("#lista-tweets");
let tweets = [];

//* Event listeners
eventListeners();
function eventListeners() {
    formulario.addEventListener("submit", agregarTweet);

    document.addEventListener("DOMContentLoaded", () => {
        tweets = JSON.parse(localStorage.getItem("tweets")) || [];
        crearHTML();
    });
}

//* Funciones
function agregarTweet(e) {
    e.preventDefault();
    const tweet = document.querySelector("#tweet").value;
    if (tweet === "") {
        mostrarError("El tweet no debe ir vacío");
        return;
    }
    // Añadir a tweets
    const tweetObj = {
        id: Date.now(),
        tweet,
    };
    tweets = [...tweets, tweetObj];
    console.log(tweets);
    crearHTML();
    formulario.reset();
}

function mostrarError(error) {
    const mensajeError = document.createElement("p");
    mensajeError.textContent = error;
    mensajeError.classList.add("error");
    const contenido = document.querySelector("#contenido");
    contenido.appendChild(mensajeError);

    // ? eliminar la alerta despues de 3 segundos
    setTimeout(() => {
        mensajeError.remove();
    }, 3000);
}

function crearHTML() {
    limpiarHTML();
    if (tweets.length > 0) {
        tweets.forEach((t) => {
            const btnEliminar = document.createElement("a");
            btnEliminar.classList.add("borrar-tweet");
            btnEliminar.innerHTML = "X";
            btnEliminar.onclick = () => {
                borrarTweet(t.id);
            };
            const li = document.createElement("li");
            li.innerText = t.tweet;
            li.appendChild(btnEliminar);
            listaTweets.appendChild(li);
        });
    }
    sincronizarStorage();
}

function borrarTweet(id) {
    tweets = tweets.filter((t) => t.id !== id);
    crearHTML();
}

function sincronizarStorage() {
    localStorage.setItem("tweets", JSON.stringify(tweets));
}

function limpiarHTML() {
    while (listaTweets.firstChild) {
        listaTweets.removeChild(listaTweets.firstChild);
    }
}
